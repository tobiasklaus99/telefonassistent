const axios = require("axios");

async function getBitcoinPrice() {
  try {
    const response = await axios.get(
      "https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=usd"
    );
    const price = response.data.bitcoin.usd;
    console.log(price);
    return JSON.stringify({ price });
  } catch (error) {
    console.error("Error fetching Bitcoin price:", error);
    return "Sorry, there was an error fetching the Bitcoin price.";
  }
}

module.exports = getBitcoinPrice;
