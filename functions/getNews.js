const axios = require("axios");

async function getNews() {
  try {
    const response = await axios.get(
      "https://www.tagesschau.de/api2u/news?regions=12"
    );
    const news = response.data.news;

    // get first 5 news items
    let newsItems = news.slice(0, 3);
    let newsToReturn = [];

    for (const newsItem of newsItems) {
      const item = {
        title: newsItem.title,
        //details: newsItem.firstSentence,
      };
      newsToReturn.push(item);
    }

    return JSON.stringify({ news: newsToReturn });
  } catch (error) {
    console.error("Error fetching News:", error);
    return "Sorry, there was an error fetching the News.";
  }
}

module.exports = getNews;
