const EventEmitter = require("events");
const uuid = require("uuid");
const WaveFile = require("wavefile").WaveFile;
const getFileHeaders = require("wav-headers");

function chunkArray(array, chunkSize) {
  const chunkedArray = [];
  for (let i = 0; i < array.length; i += chunkSize) {
    chunkedArray.push(array.slice(i, i + chunkSize));
  }
  return chunkedArray;
}

class Stream extends EventEmitter {
  constructor(websocket) {
    super();
    this.ws = websocket;
    this.audioQueue = [];
    this.processing = false;
    this.cancel = false;
  }

  async sendAudio(audio) {
    console.log("Sending audio", this.cancel);
    this.audioQueue.push(audio);
    if (!this.processing) {
      this.processing = true;
      await this.processAudioQueue();
    }
  }

  async processAudioQueue() {
    while (this.audioQueue.length > 0 && !this.cancel) {
      const audio = this.audioQueue.shift();
      await this.sendAudioChunk(audio);
    }
    if (this.cancel) {
      this.audioQueue = [];
      this.cancel = false;
      console.log("Cleared audio queue");
    }
    this.processing = false;
  }

  async sendAudioChunk(audio) {
    const options = {
      channels: 1,
      sampleRate: 24000,
      bitDepth: 16,
      dataLength: audio.length,
    };
    const headersBuffer = getFileHeaders(options);
    const fullBuffer = Buffer.concat([headersBuffer, audio]);
    const wav = new WaveFile(fullBuffer);
    wav.toSampleRate(16000);
    wav.toBitDepth("16");
    const samples = chunkArray(wav.getSamples(), 320);

    for (let i = 0; i < samples.length; i++) {
      if (this.cancel) {
        break;
      }

      let sample = samples[i];
      if (sample.length < 320) {
        console.log("Add empty samples");
        // Pad the last frame with empty samples
        const paddedSample = new Array(320).fill(0);
        sample.forEach((value, index) => {
          paddedSample[index] = value;
        });
        sample = paddedSample;
      }

      try {
        this.ws.send(Uint16Array.from(sample).buffer);
        await new Promise((resolve) => setTimeout(resolve, 15));
      } catch (error) {
        console.error("Error sending audio frame:", error);
        break;
      }
    }

    if (!this.cancel) {
      const markLabel = uuid.v4();
      this.emit("audiosent", markLabel);
    }
  }

  async sendAudioChunk2(audio) {
    const options = {
      channels: 1,
      sampleRate: 24000,
      bitDepth: 16,
      dataLength: audio.length,
    };
    const headersBuffer = getFileHeaders(options);
    const fullBuffer = Buffer.concat([headersBuffer, audio]);
    const wav = new WaveFile(fullBuffer);
    wav.toSampleRate(16000);
    wav.toBitDepth("16");
    const samples = chunkArray(wav.getSamples(), 320);
    for (const sample of samples) {
      if (this.cancel) {
        console.log("Stopping audio - no further chunks");
        break;
      }
      this.ws.send(Uint16Array.from(sample).buffer);
      await new Promise((resolve) => setTimeout(resolve, 15));
    }
    if (!this.cancel) {
      const markLabel = uuid.v4();
      this.emit("audiosent", markLabel);
    }
  }

  async sendAudioChunk2(audio) {
    const options = {
      channels: 1,
      sampleRate: 24000,
      bitDepth: 16,
      dataLength: audio.length,
    };
    const headersBuffer = getFileHeaders(options);
    const fullBuffer = Buffer.concat([headersBuffer, audio]);
    const wav = new WaveFile(fullBuffer);
    wav.toSampleRate(16000);
    wav.toBitDepth("16");
    const samples = chunkArray(wav.getSamples(), 320);
    for (const sample of samples) {
      this.ws.send(Uint16Array.from(sample).buffer);
      await new Promise((resolve) => setTimeout(resolve, 15));
    }
    const markLabel = uuid.v4();
    this.emit("audiosent", markLabel);
  }

  stopAudio() {
    if (!this.processing) {
      return;
    }
    this.cancel = true;
  }
}

module.exports = { Stream };
