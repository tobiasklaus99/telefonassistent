const EventEmitter = require("events");
const fetch = require("node-fetch");
const numToWordsDe = require("num-words-de");

class TTS extends EventEmitter {
  constructor(config) {
    super();
    this.config = config;
    this.config.voiceId ||= process.env.ELEVENLABS_VOICE_ID;
    this.nextExpectedIndex = 0;
    this.speechBuffer = {};
  }

  convertNumbersToWords(text) {
    return text.replace(/\d+(\.\d+)?/g, (match) => {
      const number = parseFloat(match.replace(/\./g, ""));
      return numToWordsDe.numToWord(number);
    });
  }

  async generate(gptReply, interactionCount) {
    const { partialResponseIndex, partialResponse } = gptReply;
    if (!partialResponse) {
      console.log(gptReply);
      return console.log("No partial response");
    }
    try {
      const outputFormat = "pcm_16000";
      const textWithNumbersConverted =
        this.convertNumbersToWords(partialResponse);
      const response = await fetch(
        `https://api.elevenlabs.io/v1/text-to-speech/${process.env.ELEVENLABS_VOICE_ID}/stream?output_format=${outputFormat}&optimize_streaming_latency=3`,
        {
          method: "POST",
          headers: {
            "xi-api-key": process.env.XI_API_KEY,
            "Content-Type": "application/json",
            accept: "audio/wav",
          },
          body: JSON.stringify({
            model_id: process.env.XI_MODEL_ID,
            text: textWithNumbersConverted,
          }),
        }
      );
      const audioArrayBuffer = await response.arrayBuffer();
      this.emit(
        "speech",
        partialResponseIndex,
        Buffer.from(audioArrayBuffer),
        partialResponse,
        interactionCount
      );
    } catch (err) {
      console.error("Error occurred in TextToSpeech service");
      console.error(err);
    }
  }
}

module.exports = { TTS };
