const EventEmitter = require("events");
const fetch = require("node-fetch");
const numToWordsDe = require("num-words-de");
const OpenAI = require("openai");
const fs = require("fs");
const path = require("path");

const openai = new OpenAI();

async function streamToFile(stream, path) {
  const options = {
    channels: 1,
    sampleRate: 24000,
    bitDepth: 16,
    dataLength: audio.length,
  };
  const headersBuffer = getFileHeaders(options);
  const fullBuffer = Buffer.concat([headersBuffer, audio]);
  const wav = new WaveFile(fullBuffer);
  wav.toSampleRate(16000);
  wav.toBitDepth("16");

  return new Promise((resolve, reject) => {
    const writeStream = fs
      .createWriteStream(path)
      .on("error", reject)
      .on("finish", resolve);

    stream.pipe(writeStream).on("error", (error) => {
      writeStream.close();
      reject(error);
    });
  });
}

class TTS3 extends EventEmitter {
  constructor(config) {
    super();
  }

  convertNumbersToWords(text) {
    return text.replace(/\d+(\.\d+)?/g, (match) => {
      const number = parseFloat(match.replace(/\./g, ""));
      return numToWordsDe.numToWord(number);
    });
  }
  connect() {
    console.log("Connecting to OpenAI");
  }

  stopAudio() {
    console.log("Stopping audio");
  }

  async sendTexts(textChunks) {
    try {
      const convertedChunks = textChunks.map((chunk) =>
        this.convertNumbersToWords(chunk)
      );

      const chunkPromises = convertedChunks.map((chunk) =>
        openai.audio.speech.create({
          model: "tts-1",
          voice: "fable",
          input: chunk,
          response_format: "pcm",
        })
      );

      const responses = await Promise.all(chunkPromises);

      for (const response of responses) {
        const audioArrayBuffer = await response.arrayBuffer();
        this.emit("audioChunk", Buffer.from(audioArrayBuffer));
      }
    } catch (err) {
      console.error("Error occurred in TextToSpeech service");
      console.error(err);
    }
  }

  async sendText(text) {
    try {
      const textWithNumbersConverted = this.convertNumbersToWords(text);
      const response = await openai.audio.speech.create({
        model: "tts-1",
        voice: "fable",
        input: textWithNumbersConverted,
        response_format: "pcm",
      });

      const audioArrayBuffer = await response.arrayBuffer();
      this.emit("audioChunk", Buffer.from(audioArrayBuffer));
    } catch (err) {
      console.error("Error occurred in TextToSpeech service");
      console.error(err);
    }
  }
}

module.exports = { TTS3 };
