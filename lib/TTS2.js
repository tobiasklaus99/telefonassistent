const EventEmitter = require("events");
const WebSocket = require("ws");
const voiceId = process.env.ELEVENLABS_VOICE_ID;
const model = process.env.XI_MODEL_ID;
const wsUrl = `wss://api.elevenlabs.io/v1/text-to-speech/${voiceId}/stream-input?model_id=${model}&output_format=pcm_16000&optimize_streaming_latency=3`;

class TTS2 extends EventEmitter {
  constructor() {
    super();
    this.socket = null;
    this.unsentText = [];
    this.connecting = false;
  }

  connect() {
    this.connecting = true;
    this.socket = new WebSocket(wsUrl);

    this.socket.onopen = (event) => {
      this.connecting = false;
      const bosMessage = {
        text: " ",
        voice_settings: {
          stability: 0.5,
          similarity_boost: 0.8,
        },
        xi_api_key: process.env.XI_API_KEY,
      };
      this.socket.send(JSON.stringify(bosMessage));

      // Send unsent text when the socket is opened
      this.unsentText.forEach((text) => {
        this.sendText(text);
      });
      this.unsentText = [];
    };

    this.socket.onmessage = (event) => {
      const response = JSON.parse(event.data);

      if (response.audio) {
        const audioData = atob(response.audio);
        const audioArrayBuffer = new ArrayBuffer(audioData.length);
        const audioUint8Array = new Uint8Array(audioArrayBuffer);
        for (let i = 0; i < audioData.length; i++) {
          audioUint8Array[i] = audioData.charCodeAt(i);
        }
        console.log("Received audio chunk");
        this.emit("audioChunk", Buffer.from(audioUint8Array));
      } else {
        console.log("No audio data in the response");
      }

      if (response.isFinal) {
        this.emit("generationComplete");
      }

      if (response.normalizedAlignment) {
        this.emit("alignmentInfo", response.normalizedAlignment);
      }
    };

    this.socket.onerror = (error) => {
      console.error(`WebSocket Error: ${error}`);
      this.emit("error", error);
    };

    this.socket.onclose = (event) => {
      if (event.wasClean) {
        console.info(
          `Connection closed cleanly, code=${event.code}, reason=${event.reason}`
        );
      } else {
        console.warn("Connection died");
      }
      this.emit("close", event);
    };
  }

  sendText(text) {
    if (this.socket && this.socket.readyState === WebSocket.OPEN) {
      const textMessage = {
        text: text,
        try_trigger_generation: true,
      };
      this.socket.send(JSON.stringify(textMessage));

      const eosMessage = {
        text: "",
      };
      this.socket.send(JSON.stringify(eosMessage));
    } else {
      console.warn("WebSocket connection is not open. Storing text for later.");
      this.unsentText.push(text);

      if (!this.connecting) {
        this.connect();
      }
    }
  }

  stopAudio() {
    this.socket.close();
    this.unsentText = [];
  }

  disconnect() {
    if (this.socket) {
      this.socket.close();
    }
  }
}

module.exports = { TTS2 };

//const tts = new TTS2();
//tts.connect();
//tts.sendText("Hallo! Was willst du besprechen?");
//tts.on("audioChunk", (audioChunk) => {
//  console.log("Audio chunk received", audioChunk);
//});
//tts.on("generationComplete", () => {
//  console.log("Generation complete");
//});
