const EventEmitter = require("events");
const { Vonage } = require("@vonage/server-sdk");

const vonage = new Vonage(
  {
    apiKey: process.env.VONAGE_API_KEY,
    apiSecret: process.env.VONAGE_API_SECRET,
    applicationId: process.env.VONAGE_APPLICATION_ID,
    privateKey: process.env.VONAGE_PRIVATE_KEY,
  },
  { debug: true }
);

class Phone extends EventEmitter {
  constructor(uuid) {
    super();
    this.uuid = uuid;
  }

  setStreamSid(streamSid) {
    this.streamSid = streamSid;
  }

  buffer(index, audio) {
    if (index === null) {
      this.sendAudio(audio);
    } else if (index === this.expectedAudioIndex) {
      this.sendAudio(audio);
      this.expectedAudioIndex++;
      while (this.audioBuffer.hasOwnProperty(this.expectedAudioIndex)) {
        const bufferedAudio = this.audioBuffer[this.expectedAudioIndex];
        this.sendAudio(bufferedAudio);
        delete this.audioBuffer[this.expectedAudioIndex];
        this.expectedAudioIndex++;
      }
    } else {
      this.audioBuffer[index] = audio;
    }
  }

  sendAudio() {
    vonage.voice
      .streamAudio(
        this.uuid,
        "https://nexmo-community.github.io/ncco-examples/assets/voice_api_audio_streaming.mp3",
        0
      )
      .then((resp) => console.log(resp))
      .catch((err) => console.error(err));

    return;

    const options = {
      channels: 1,
      sampleRate: 16000,
      bitDepth: 16,
      dataLength: audio.length,
    };
    const headersBuffer = getFileHeaders(options);
    const fullBuffer = Buffer.concat([headersBuffer, audio]);
    const wav = new WaveFile(fullBuffer);
    wav.toSampleRate(16000);
    wav.toBitDepth("16");
    const samples = chunkArray(wav.getSamples(), 320);
    for (const sample of samples) {
      this.ws.send(Uint16Array.from(sample).buffer);
    }
    const markLabel = uuid.v4();
    this.emit("audiosent", markLabel);
  }

  stopAudio() {
    vonage.voice
      .stopStreamAudio(this.uuid)
      .then((resp) => console.log(resp))
      .catch((err) => console.error(err));
  }
}

module.exports = { Phone };
