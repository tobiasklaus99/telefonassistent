const EventEmitter = require("events");
const colors = require("colors");
const OpenAI = require("openai");
//const tools = require('../functions/function-manifest');

const getTextChunks = (text) => {
  // Split the text into sentences using regular expression
  const sentences = text.match(/[^.!?]+[.!?]+/g);

  const chunks = [];
  let currentChunk = "";

  sentences.forEach((sentence) => {
    // If the current chunk is empty or the combined length is less than 200 characters,
    // append the sentence to the current chunk
    if (currentChunk === "" || currentChunk.length + sentence.length <= 200) {
      currentChunk += sentence;
    } else {
      // If the combined length exceeds 200 characters,
      // push the current chunk to the chunks array and start a new chunk
      chunks.push(currentChunk.trim());
      currentChunk = sentence;
    }
  });

  // Push the last chunk if it's not empty
  if (currentChunk !== "") {
    chunks.push(currentChunk.trim());
  }

  return chunks;
};

const tools = [
  {
    type: "function",
    function: {
      name: "getBitcoinPrice",
      description: "Retrieves the current Bitcoin price in USD.",
      parameters: {
        type: "object",
        properties: {},
        required: [],
      },
      returns: {
        type: "object",
        properties: {
          price: {
            type: "number",
            description: "The current Bitcoin price in USD.",
          },
        },
      },
    },
  },
  {
    type: "function",
    function: {
      name: "getNews",
      description: "Retrieves the latest news headlines for Saarland.",
      parameters: {
        type: "object",
        properties: {},
        required: [],
      },
      returns: {
        type: "object",
        properties: {
          news: {
            type: "array",
            items: {
              type: "object",
              properties: {
                title: {
                  type: "string",
                  description: "The title of the news item.",
                },
                //details: {
                //  type: "string",
                //  description: "A brief summary of the news item.",
                //},
              },
            },
          },
        },
      },
    },
  },
];

// Import all functions included in function manifest
// Note: the function name and file name must be the same
const availableFunctions = {};
tools.forEach((tool) => {
  const functionName = tool.function.name;
  availableFunctions[functionName] = require(`../functions/${functionName}`);
});

class LLM extends EventEmitter {
  constructor() {
    super();
    this.openai = new OpenAI();
    (this.userContext = [
      {
        role: "system",
        content: `You are a person on a phone. You are friendly.
        Keep your responses as brief as possible but make every attempt to keep the caller on the phone without being rude.
        Don't ask more than 1 question at a time. Ask for clarification if a user request is ambiguous.
        Always respond in German and keep it short. If the user asks for the current Bitcoin price, call the getBitcoinPrice function to retrieve it.
        If the user asks for the latest news headlines (for Saarland), call the getNews function to retrieve them.
        Always respond in German, also for the news headlines. Summarize the news headlines in German and keep it short.`,
      },
      {
        role: "assistant",
        content: "Hallo! Was willst du besprechen?",
      },
    ]),
      (this.partialResponseIndex = 0);
  }

  async completion(text, interactionCount, role = "user", name = "user") {
    if (name != "user") {
      this.userContext.push({ role: role, name: name, content: text });
    } else {
      this.userContext.push({ role: role, content: text });
    }

    console.log(this.userContext);

    // Step 1: Send user transcription to Chat GPT
    const stream = await this.openai.chat.completions.create({
      // model: "gpt-4-1106-preview",
      model: "gpt-3.5-turbo",
      messages: this.userContext,
      tools: tools,
      stream: true,
    });

    let completeResponse = "";
    let functionName = "";
    let functionArgs = "";
    let finishReason = "";

    for await (const chunk of stream) {
      let content = chunk.choices[0]?.delta?.content || "";
      let deltas = chunk.choices[0].delta;

      // Step 2: check if GPT wanted to call a function
      if (deltas.tool_calls) {
        // Step 3: call the function
        let name = deltas.tool_calls[0]?.function?.name || "";
        if (name != "") {
          functionName = name;
        }
        let args = deltas.tool_calls[0]?.function?.arguments || "";
        if (args != "") {
          // args are streamed as JSON string so we need to concatenate all chunks
          functionArgs += args;
        }
      }
      // check to see if it is finished
      finishReason = chunk.choices[0].finish_reason;

      // need to call function on behalf of Chat GPT with the arguments it parsed from the conversation
      if (finishReason === "tool_calls") {
        // parse JSON string of args into JSON object
        try {
          functionArgs = JSON.parse(functionArgs);
        } catch (error) {
          // was seeing an error where sometimes we have two sets of args
          if (functionArgs.indexOf("{") != functionArgs.lastIndexOf("{"))
            functionArgs = JSON.parse(
              functionArgs.substring(
                functionArgs.indexOf(""),
                functionArgs.indexOf("}") + 1
              )
            );
        }

        const functionToCall = availableFunctions[functionName];
        let functionResponse = await functionToCall(functionArgs);

        // Step 4: send the info on the function call and function response to GPT
        this.userContext.push({
          role: "function",
          name: functionName,
          content: functionResponse,
        });
        // extend conversation with function response

        // call the completion function again but pass in the function response to have OpenAI generate a new assistant response
        await this.completion(
          functionResponse,
          interactionCount,
          "function",
          functionName
        );
      } else {
        // We use completeResponse for userContext
        completeResponse += content;
        // Emit last partial response and add complete response to userContext
        if (content.length > 0) {
          //this.emit("gptreply", content);
        }
        if (finishReason === "stop") {
          this.emit("gptfinish");
          this.emit("gptreply", getTextChunks(completeResponse));
        }
      }
    }
    this.userContext.push({ role: "assistant", content: completeResponse });
    console.log(`GPT -> user context length: ${this.userContext.length}`.green);
  }
}

module.exports = { LLM };
