require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const WebSocket = require("ws");
const { Vonage } = require("@vonage/server-sdk");
const { TTS } = require("./lib/TTS");
const { TTS2 } = require("./lib/TTS2");
const { TTS3 } = require("./lib/TTS3");
const { Transcribe } = require("./lib/Transcribe");
const { LLM } = require("./lib/LLM");
const { Stream } = require("./lib/Stream");
const { Phone } = require("./lib/Phone");

const wss = new WebSocket.Server({ port: 8080 });

app.use(bodyParser.json());

app.get("/answer", (req, res) => {
  let nccoResponse = [
    {
      action: "connect",
      endpoint: [
        {
          type: "websocket",
          "content-type": "audio/l16;rate=16000",
          uri: `wss://nexmo.planpraktisch.de/socket`,
          headers: {
            language: "en-GB",
            "caller-id": req.query.from,
            uuid: req.query.uuid,
          },
        },
      ],
    },
  ];

  res.status(200).json(nccoResponse);
});

app.post("/event", (req, res) => {
  console.log("EVENT LOG::", req.body);
  res.status(204).end();
});

wss.on("connection", (ws, req) => {
  ws.on("error", console.error);

  ws.on("close", (event) => {
    console.log("WebSocket connection closed", event);
  });

  console.log(req);

  const llm = new LLM();
  const stream = new Stream(ws);
  const transcribe = new Transcribe();
  const tts = new TTS3();
  let uuid = null;
  let callerId = null;
  //const phone = new Phone(uuidv4());
  let phone = null;

  let marks = [];

  ws.once("message", function message(msg) {
    // check if msg is json and if it is json, parse it
    if (typeof msg === "string") {
      try {
        msg = JSON.parse(msg);
        callerId = msg["caller-id"];
        uuid = msg.uuid;
        phone = new Phone(uuid);
      } catch (err) {
        console.error("Error parsing JSON message:", err);
        return;
      }
    }
  });

  // Incoming from MediaStream
  ws.on("message", function message(msg) {
    transcribe.send(msg);
  });

  transcribe.on("utterance", async (text) => {
    // This is a bit of a hack to filter out empty utterances
    //if (marks.length > 0 && text?.length > 5) {
    //  console.log("Twilio -> Interruption, Clearing stream".red);
    //  //phone.stopAudio(uuid);
    //  //tts.stopAudio();
    //  //stream.stopAudio();
    //}
    if (text?.length > 2) {
      console.log("Twilio -> Interruption, Clearing stream".red);
      //phone.stopAudio(uuid);
      //tts.stopAudio();
      stream.stopAudio();
    }
  });

  transcribe.on("transcription", async (text) => {
    if (!text) {
      return;
    }
    console.log(`Interaction – STT -> GPT: ${text}`.yellow);
    llm.completion(text);
  });

  llm.on("gptreply", async (gptReply) => {
    console.log(`Interaction: GPT -> TTS: ${gptReply}`.green);
    tts.sendTexts(gptReply);
  });

  llm.on("gptfinish", async () => {
    // finished
  });

  tts.on("audioChunk", (audio) => {
    console.log(`Interaction: TTS -> TWILIO`.blue);

    stream.sendAudio(audio);
  });

  stream.on("audiosent", (markLabel) => {
    marks.push(markLabel);
  });

  tts.sendTexts(["Hallo! Was willst du besprechen?"]);
});

const port = process.env.PORT || 8000;
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
